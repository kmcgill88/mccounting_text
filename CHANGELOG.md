## 1.0.0
- Upgrade to null safety
- Upgrade to Dart 2.12 minimum
- Add example project

## 0.1.0

* Initial Open Source release.
