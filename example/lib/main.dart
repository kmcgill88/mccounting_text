import 'package:flutter/material.dart';
import 'package:mccounting_text/mccounting_text.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(widget.title),),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text('You have pushed the button this many times:',),
            McCountingText(
              begin: 0,
              end: _counter.toDouble(),
              style: Theme.of(context).textTheme.headline2,
              duration: const Duration(seconds: 1),
              curve: Curves.decelerate,
            ),
            const Text('Set your own duration and curve:',),
            McCountingText(
              begin: 0,
              end: _counter.toDouble(),
              precision: 1,
              style: Theme.of(context).textTheme.headline2,
              duration: const Duration(seconds: 3),
              curve: Curves.linear,
            ),
            const Text('Choose precision between 0-20:',),
            McCountingText(
              begin: 0,
              end: _counter.toDouble(),
              precision: 2,
              style: Theme.of(context).textTheme.headline2,
              duration: const Duration(seconds: 1),
              curve: Curves.fastOutSlowIn,
            ),
            const Text('Count down:',),
            McCountingText(
              begin: _counter.toDouble(),
              end: 0,
              precision: 3,
              style: Theme.of(context).textTheme.headline2,
              duration: const Duration(seconds: 1),
              curve: Curves.ease,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ),
    );
  }
}
