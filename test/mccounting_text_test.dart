import 'package:flutter_test/flutter_test.dart';

import 'package:mccounting_text/mccounting_text.dart';
import 'package:flutter/material.dart';

void main() {
  testWidgets('McCountingText has a begin and end', (WidgetTester tester) async {
    await tester.pumpWidget(const MaterialApp(home: McCountingText(begin: 0, end: 0,)));

    final foundZero = find.text("0");
    expect(foundZero, findsOneWidget);
  });

  testWidgets('McCountingText renders to end', (WidgetTester tester) async {
    await tester.pumpWidget(const MaterialApp(home: McCountingText(begin: 0, end: 5,)));
    await tester.pumpAndSettle();
    
    final found = find.text("5");
    expect(found, findsOneWidget);
  });
}
