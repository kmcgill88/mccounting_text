# mccounting_text
[![pipeline status](https://gitlab.com/kmcgill88/mccounting_text/badges/master/pipeline.svg)](https://gitlab.com/kmcgill88/mccounting_text/commits/master)
[![MIT license](https://img.shields.io/badge/License-MIT-blue.svg)](https://lbesson.mit-license.org/)
[![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)](http://makeapullrequest.com)

## About
An animated Text widget that counts between two numbers on a duration, curve and style you specify.

## Usage
![mccounting-text](https://mcgilldevtech.com/img/gitlab/mccounting-text/mccounting_text.gif)

## Basic
Give `McCountingText` a begin and end and it will animate the count between those two numbers. You can count up or down, positive to negitive, including decimals.
```dart
McCountingText(
    begin: 0,
    end: 30,
)
```

## Gif Examples
```dart
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(widget.title),),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('You have pushed the button this many times:',),
            McCountingText(
              begin: 0,
              end: _counter.toDouble(),
              style: Theme.of(context).textTheme.headline2,
              duration: Duration(seconds: 1),
              curve: Curves.decelerate,
            ),
            Text('Set your own duration and curve:',),
            McCountingText(
              begin: 0,
              end: _counter.toDouble(),
              precision: 1,
              style: Theme.of(context).textTheme.headline2,
              duration: Duration(seconds: 3),
              curve: Curves.linear,
            ),
            Text('Choose precision between 0-20:',),
            McCountingText(
              begin: 0,
              end: _counter.toDouble(),
              precision: 2,
              style: Theme.of(context).textTheme.headline2,
              duration: Duration(seconds: 1),
              curve: Curves.fastOutSlowIn,
            ),
            Text('Count down:',),
            McCountingText(
              begin: _counter.toDouble(),
              end: 0,
              precision: 3,
              style: Theme.of(context).textTheme.headline2,
              duration: Duration(seconds: 1),
              curve: Curves.ease,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }
```